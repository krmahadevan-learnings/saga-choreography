package com.vinsguru.order.service;

import com.vinsguru.order.entity.PurchaseOrder;
import com.vinsguru.order.repository.PurchaseOrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderQueryService {
    private final PurchaseOrderRepository purchaseOrderRepository;
    public List<PurchaseOrder> getAll() {
        return this.purchaseOrderRepository.findAll();
    }

}
