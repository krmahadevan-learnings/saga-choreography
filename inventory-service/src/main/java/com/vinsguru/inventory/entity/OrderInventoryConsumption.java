package com.vinsguru.inventory.entity;

import com.vinsguru.events.order.OrderEvent;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class OrderInventoryConsumption {

    @Id
    private UUID orderId;
    private int productId;
    private int quantityConsumed;

    public static OrderInventoryConsumption of(OrderEvent orderEvent, int quantityConsumed) {
        OrderInventoryConsumption consumption = new OrderInventoryConsumption();
        consumption.orderId = orderEvent.getPurchaseOrder().getOrderId();
        consumption.productId = orderEvent.getPurchaseOrder().getProductId();
        consumption.quantityConsumed = quantityConsumed;
        return consumption;
    }

}
