package com.vinsguru.inventory.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
@NoArgsConstructor
public class OrderInventory {

    @Id
    private int productId;
    private int availableInventory;

    public void decrementAvailableInventory() {
        availableInventory -= 1;
    }

    public void incrementInventoryWith(int quantity) {
        availableInventory += quantity;
    }
    public boolean inventoryAvailable() {
        return availableInventory > 0;
    }

}
