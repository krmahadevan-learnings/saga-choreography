package com.vinsguru.inventory.service;

import com.vinsguru.dto.InventoryDto;
import com.vinsguru.events.inventory.InventoryEvent;
import com.vinsguru.events.inventory.InventoryStatus;
import com.vinsguru.events.order.OrderEvent;
import com.vinsguru.inventory.entity.OrderInventory;
import com.vinsguru.inventory.entity.OrderInventoryConsumption;
import com.vinsguru.inventory.repository.OrderInventoryConsumptionRepository;
import com.vinsguru.inventory.repository.OrderInventoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class InventoryService {

    private final OrderInventoryRepository inventoryRepository;

    private final OrderInventoryConsumptionRepository consumptionRepository;

    @Transactional
    public InventoryEvent newOrderInventory(OrderEvent orderEvent) {
        InventoryDto dto = InventoryDto.of(orderEvent.getPurchaseOrder().getOrderId(), orderEvent.getPurchaseOrder().getProductId());
        return inventoryRepository.findById(orderEvent.getPurchaseOrder().getProductId())
                .filter(OrderInventory::inventoryAvailable)
                .map(i -> {
                    i.decrementAvailableInventory();
                    consumptionRepository.save(OrderInventoryConsumption.of(orderEvent, 1));
                    return new InventoryEvent(dto, InventoryStatus.RESERVED);
                })
                .orElse(new InventoryEvent(dto, InventoryStatus.REJECTED));
    }

    @Transactional
    public void cancelOrderInventory(OrderEvent orderEvent) {
        consumptionRepository.findById(orderEvent.getPurchaseOrder().getOrderId())
                .ifPresent(ci -> {
                            inventoryRepository.findById(ci.getProductId())
                                    .ifPresent(i -> i.incrementInventoryWith(ci.getQuantityConsumed()));
                            consumptionRepository.delete(ci);
                        }
                );
    }

}
