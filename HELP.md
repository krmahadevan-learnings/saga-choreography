# Saga Pattern - Orchestration

This is a sample project to demo saga pattern.

## Prerequisites:

* Kafka cluster
* use the docker-compose file for setting up the cluster

# High Level Architecture

![](doc/saga-choreography.png)

## Disclaimer from Krishnan

This project is a complete copy of the original work from [Vinoth Selvaraj](https://github.com/vinsguru)

The original work is available on [Github](https://github.com/vinsguru/vinsguru-blog-code-samples/tree/master/architectural-pattern/saga-choreography)

The only reason why this  repo exists separately is because its easy for me to find it when I need to refer it.


