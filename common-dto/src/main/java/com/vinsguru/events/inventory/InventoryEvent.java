package com.vinsguru.events.inventory;

import com.vinsguru.dto.InventoryDto;
import com.vinsguru.events.Event;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

import java.util.Date;
import java.util.UUID;

@RequiredArgsConstructor
public class InventoryEvent implements Event {
    private final UUID eventId = UUID.randomUUID();
    private final Date date = new Date();
    @Getter
    private final InventoryDto inventory;
    @Getter
    private final InventoryStatus status;

    @Override
    public UUID getEventId() {
        return this.eventId;
    }

    @Override
    public Date getDate() {
        return this.date;
    }

}
