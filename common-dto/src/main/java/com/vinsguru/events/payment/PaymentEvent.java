package com.vinsguru.events.payment;

import com.vinsguru.dto.PaymentDto;
import com.vinsguru.events.Event;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Date;
import java.util.UUID;

@RequiredArgsConstructor
public class PaymentEvent implements Event {

    private final UUID eventId = UUID.randomUUID();
    private final Date date = new Date();
    @Getter
    private final PaymentDto payment;
    @Getter
    private final PaymentStatus paymentStatus;

    @Override
    public UUID getEventId() {
        return this.eventId;
    }

    @Override
    public Date getDate() {
        return this.date;
    }
}
