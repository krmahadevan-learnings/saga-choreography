package com.vinsguru.events.order;

import com.vinsguru.dto.PurchaseOrderDto;
import com.vinsguru.events.Event;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Date;
import java.util.UUID;

@RequiredArgsConstructor
public class OrderEvent implements Event {

    private final UUID eventId = UUID.randomUUID();
    private final Date date = new Date();
    @Getter
    private final PurchaseOrderDto purchaseOrder;
    @Getter
    private final OrderStatus orderStatus;

    @Override
    public UUID getEventId() {
        return this.eventId;
    }

    @Override
    public Date getDate() {
        return this.date;
    }

}
